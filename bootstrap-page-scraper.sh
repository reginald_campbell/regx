#! /bin/bash
wget --no-check-certificate https://bootstrap.pypa.io/ez_setup.py -O - | sudo python
sudo wget --no-check-certificate https://raw.github.com/pypa/pip/master/contrib/get-pip.py
sudo yum install -y libxslt-devel libxml2-devel
sudo yum install -y python-devel 
sudo python get-pip.py
sudo pip install -U boto
sudo pip install -U lxml
sudo easy_install beautifulsoup4
