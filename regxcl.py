#! /usr/bin/python

import sys, urllib2, boto, time
from bs4 import BeautifulSoup, NavigableString
from boto.sqs.message import Message
from datetime import datetime

queue = 'emr_logs'
key = 'AKIAI4NWMET6DTYHLJLQ'
sec = 'lVYEX1+I0US4kSEs+qoSJn+zVve+zkul03cFVWbg'
bucket = 'ndn-emr'

sqs = boto.connect_sqs(key, sec)
q = sqs.get_queue(queue)

#logs execptions to AWS SQS
def log(etype, url, e, q):
       
        d = getUTCNow() + "\t" + etype + "\t" + e + "\t" + url
        m = Message()
        m.set_body(d)

        q.write(m)

#getUTCNow
def getUTCNow():
        dt = datetime.utcnow()
	return datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

def printOp(urlMsg):
	print getUTCNow()+"\t"+urlMsg
	
#saves output to S3        
def regxcl(url):
	try:
		hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
			'Accept-Encoding': 'none',
			'Accept-Language': 'en-US,en;q=0.8',
			'Connection': 'keep-alive'
		}

		req = urllib2.Request(url, headers=hdr)

		sock = urllib2.urlopen(req)
		printOp("%s responded back with HTTP " % url + str(sock.getcode()) + "\n\n")
		htmlSource = sock.read()
		sock.close()
		cleanhtml=BeautifulSoup(htmlSource)
		ndnstuff = cleanhtml.find_all('iframe', src=True) + cleanhtml.find_all('script') + cleanhtml.find_all('div', class_="ndn_embed") + cleanhtml.find_all('div', class_="ndn-widget")

		for x in ndnstuff:
			if "http://launch.newsinc.com/js/embed.js" in str(x) or "//launch.newsinc.com/js/embed.js" in str(x) or "http://www.launch.newsinc.com/js/embed.js" in str(x) or "https://www.launch.newsinc.com/js/embed.js" in str(x) or "https://launch.newsinc.com/js/embed.js" in str(x):
				printOp(url+"\tEmbed.js call: " + str(x))
			elif "http://dmeserv.newsinc.com/repub/" in str(x):
				printOp(url+"\tOld PP dynamic widget call: " + str(x))
			elif "var _ndnq" in str(x):
				printOp(url+"\tDynamic embed detected" + str(x))
			elif "VideoPlayer/Single" in str(x) or "VideoPlayer/Default" in str(x):
				printOp(url+"\tSingle player: " + str(x))
				print "made it past printOp"
				required(x, url)
			elif "VideoPlayer/Inline300" in str(x):
				printOp("\tInline300 player: " + str(x))
				required(x, url)
			elif "VideoPlayer/Inline590" in str(x):
				printOp(url+"\tInline590 player: " + str(x))
				required(x, url)
			elif "VideoLauncher/Slider300x250" in str(x):
				printOp(url+"\tSlider300x250 player: " + str(x))
				required(x, url)
			elif "VideoLauncher/Playlist300x250" in str(x):
				printOp(url+"\tPlaylist300x250 player: " + str(x))
				required(x, url)
			elif "VideoLauncher/Horizontal" in str(x):
				printOp(url+"\tHorizontal player: " + str(x))
				required(x, url)
			elif "VideoLauncher/VerticalTower" in str(x):
				printOp(url+"\tVerticalTower player: " + str(x))
				required(x, url)
			elif "ndn-video-player" in str(x):
				printOp(url+"\tPerfect Pixel placement: " + str(x))
				pprequired(x, url)
			elif "embed.newsinc.com" in str(x):
				printOp(url+"\tiframe placement: " + str(x))
			elif "ndn-video-single" in str(x):
				printOp(url+"\tOld PP placement: " + str(x))
			elif "VideoPlayer/Studio" in str(x):
				printOp(url+"\tStudio placement: " + str(x))
	except urllib2.HTTPError, e:
		log('urllib2.HTTPError', url, str(e), q)
	except urllib2.URLError, e:
		log('urllib2.URLError', url, str(e), q)
	except IOError, e:
		log('IOError', url, str(e), q)
	except AttributeError:
		log('AttributeError', url, str(e), q)
	except KeyError:
		log('KeyError', url, str(e), q)
	except TypeError:
		log('TypeError', url, str(e), q)

def required(div, url):
	if "data-config-widget-id=" not in str(div):
		printOp(url+"\t"+"Missing widgetID")
	else:
		y=div['data-config-widget-id']
		try:
			y=int(y)
		except ValueError:
			printOp(url+"\t"+"Invalid characters in widgetID --> " + y)
	if "data-config-tracking-group=" not in str(div):
		printOp(url+"\t"+"Missing tracking group")
	else:
		y=div['data-config-tracking-group']
		try:
			y=int(y)
		except ValueError:
			printOp(url+"\t"+"Invalid characters in tracking group --> " + y)
	if "data-config-width=" in str(div):
		y=div['data-config-width']
		try:
			y=int(y)
			if y < 300:
				printOp(url+"\t"+"Width " + str(y) + "px is below required miminum width of 300px")
		except ValueError:
			if "%" in y:
				printOp(url+"\t"+"Width of this div is " + y)
			else:
				printOp(url+"\t"+"Width contains invalid characters --> " + y)					
	if "data-config-height=" in str(div):
		y=div['data-config-height']
		try:
			y=int(y)
			if y < 170:
				printOp(url+"\t"+"Height " + str(y) + "px is below required miminum height of 170px")
		except ValueError:
			if "%" in y:
				printOp(url+"\t"+"Height of this div is " + y)
			else:
				printOp(url+"\t"+"Height contains invalid characters --> " + y)		
	if "data-config-playlist-id=" in str(div):
		y=div['data-config-playlist-id']
		try:
			y=int(y)
		except ValueError:
			printOp(url+"\t"+"Invalid characters in playlistID " + y)
	if "data-config-video-id=" in str(div):
		y=div['data-config-video-id']
		try:
			y=int(y)
		except ValueError:
			printOp(url+"\t"+"Invalid characters in videoID " + y)	
	if "data-config-site-section=" in str(div):
		y=div['data-config-site-section']
		try:
			z=y+"test"
		except ValueError:
			printOp(url+"\t"+"Invalid characters in site section " + str(y))
	if "</div>" not in str(div):
		printOp(url+"\t"+"Closing div tag missing.")				
#	return output_list

def pprequired(div, url):
	if "data-config-distributor-id=" not in str(div):
		printOp(url+"\t"+"Missing tracking group")
	else:
		y=div['data-config-distributor-id']
		try:
			y=int(y)
		except ValueError:
			printOp(url+"\t"+"Invalid characters in tracking group " + y)
	if "id=" not in str(div):
		printOp(url+"\t"+"Missing div ID")
	else:
		y=div['id']
		try:
			z=y+"test"
		except ValueError:
			printOp(url+"\t"+"Invalid characters in div ID " + str(y))
	if "</div>" not in str(div):
		printOp("Closing div tag missing.")
#	output_list+="\n"	

for line in sys.stdin:
	regxcl(line.strip())
	time.sleep(2)
#regxcl(sys.argv[1])
