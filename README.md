This python program was written in Python 2.7.5.  It imports urllib2
and then BeautifulSoup & NavigableString from BeautifulSoup4.  This program scraps the rendered HTML
from any page and looks for our NDN code (embed.js and player div calls).  Need to add dynamic embed check.
Script based upon 2.0 implementation guide: http://assets.newsinc.com/2013-players-suite/implementation/index.html

In order to run in terminal or cygwin type:
python regxcl.py <type_url_here>